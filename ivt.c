#include <ryukos.h>
#include "ivt.h"

void default_handler(void)
{
    assert(0, "Default handler entered.");
}

void nmi_handler(void) __attribute__((weak, alias("default_handler")));
void hardfault_handler(void) __attribute__((weak, alias("default_handler")));
void memmanage_handler(void) __attribute__((weak, alias("default_handler")));
void busfault_handler(void) __attribute__((weak, alias("default_handler")));
void usagefault_handler(void) __attribute__((weak, alias("default_handler")));
void SCU_0_hdlr(void) __attribute__((weak, alias("default_handler")));
void ERU0_0_hdlr(void) __attribute__((weak, alias("default_handler")));
void ERU0_1_hdlr(void) __attribute__((weak, alias("default_handler")));
void ERU0_2_hdlr(void) __attribute__((weak, alias("default_handler")));
void ERU0_3_hdlr(void) __attribute__((weak, alias("default_handler")));
void ERU1_0_hdlr(void) __attribute__((weak, alias("default_handler")));
void ERU1_1_hdlr(void) __attribute__((weak, alias("default_handler")));
void ERU1_2_hdlr(void) __attribute__((weak, alias("default_handler")));
void ERU1_3_hdlr(void) __attribute__((weak, alias("default_handler")));
void PMU0_0_hdlr(void) __attribute__((weak, alias("default_handler")));
void VADC0_C0_0_hdlr(void) __attribute__((weak, alias("default_handler")));
void VADC0_C0_1_hdlr(void) __attribute__((weak, alias("default_handler")));
void VADC0_C0_2_hdlr(void) __attribute__((weak, alias("default_handler")));
void VADC0_C0_3_hdlr(void) __attribute__((weak, alias("default_handler")));
void VADC0_G0_0_hdlr(void) __attribute__((weak, alias("default_handler")));
void VADC0_G0_1_hdlr(void) __attribute__((weak, alias("default_handler")));
void VADC0_G0_2_hdlr(void) __attribute__((weak, alias("default_handler")));
void VADC0_G0_3_hdlr(void) __attribute__((weak, alias("default_handler")));
void VADC0_G1_0_hdlr(void) __attribute__((weak, alias("default_handler")));
void VADC0_G1_1_hdlr(void) __attribute__((weak, alias("default_handler")));
void VADC0_G1_2_hdlr(void) __attribute__((weak, alias("default_handler")));
void VADC0_G1_3_hdlr(void) __attribute__((weak, alias("default_handler")));
void VADC0_G2_0_hdlr(void) __attribute__((weak, alias("default_handler")));
void VADC0_G2_1_hdlr(void) __attribute__((weak, alias("default_handler")));
void VADC0_G2_2_hdlr(void) __attribute__((weak, alias("default_handler")));
void VADC0_G2_3_hdlr(void) __attribute__((weak, alias("default_handler")));
void VADC0_G3_0_hdlr(void) __attribute__((weak, alias("default_handler")));
void VADC0_G3_1_hdlr(void) __attribute__((weak, alias("default_handler")));
void VADC0_G3_2_hdlr(void) __attribute__((weak, alias("default_handler")));
void VADC0_G3_3_hdlr(void) __attribute__((weak, alias("default_handler")));
void DSD0_M_0_hdlr(void) __attribute__((weak, alias("default_handler")));
void DSD0_M_1_hdlr(void) __attribute__((weak, alias("default_handler")));
void DSD0_M_2_hdlr(void) __attribute__((weak, alias("default_handler")));
void DSD0_M_3_hdlr(void) __attribute__((weak, alias("default_handler")));
void DSD0_A_4_hdlr(void) __attribute__((weak, alias("default_handler")));
void DSD0_A_5_hdlr(void) __attribute__((weak, alias("default_handler")));
void DSD0_A_6_hdlr(void) __attribute__((weak, alias("default_handler")));
void DSD0_A_7_hdlr(void) __attribute__((weak, alias("default_handler")));
void DAC0_0_hdlr(void) __attribute__((weak, alias("default_handler")));
void DAC0_1_hdlr(void) __attribute__((weak, alias("default_handler")));
void CCU40_0_hdlr(void) __attribute__((weak, alias("default_handler")));
void CCU40_1_hdlr(void) __attribute__((weak, alias("default_handler")));
void CCU40_2_hdlr(void) __attribute__((weak, alias("default_handler")));
void CCU40_3_hdlr(void) __attribute__((weak, alias("default_handler")));
void CCU41_0_hdlr(void) __attribute__((weak, alias("default_handler")));
void CCU41_1_hdlr(void) __attribute__((weak, alias("default_handler")));
void CCU41_2_hdlr(void) __attribute__((weak, alias("default_handler")));
void CCU41_3_hdlr(void) __attribute__((weak, alias("default_handler")));
void CCU42_0_hdlr(void) __attribute__((weak, alias("default_handler")));
void CCU42_1_hdlr(void) __attribute__((weak, alias("default_handler")));
void CCU42_2_hdlr(void) __attribute__((weak, alias("default_handler")));
void CCU42_3_hdlr(void) __attribute__((weak, alias("default_handler")));
void CCU43_0_hdlr(void) __attribute__((weak, alias("default_handler")));
void CCU43_1_hdlr(void) __attribute__((weak, alias("default_handler")));
void CCU43_2_hdlr(void) __attribute__((weak, alias("default_handler")));
void CCU43_3_hdlr(void) __attribute__((weak, alias("default_handler")));
void CCU80_0_hdlr(void) __attribute__((weak, alias("default_handler")));
void CCU80_1_hdlr(void) __attribute__((weak, alias("default_handler")));
void CCU80_2_hdlr(void) __attribute__((weak, alias("default_handler")));
void CCU80_3_hdlr(void) __attribute__((weak, alias("default_handler")));
void CCU81_0_hdlr(void) __attribute__((weak, alias("default_handler")));
void CCU81_1_hdlr(void) __attribute__((weak, alias("default_handler")));
void CCU81_2_hdlr(void) __attribute__((weak, alias("default_handler")));
void CCU81_3_hdlr(void) __attribute__((weak, alias("default_handler")));
void POSIF0_0_hdlr(void) __attribute__((weak, alias("default_handler")));
void POSIF0_1_hdlr(void) __attribute__((weak, alias("default_handler")));
void POSIF1_0_hdlr(void) __attribute__((weak, alias("default_handler")));
void POSIF1_1_hdlr(void) __attribute__((weak, alias("default_handler")));
void CAN0_0_hdlr(void) __attribute__((weak, alias("default_handler")));
void CAN0_1_hdlr(void) __attribute__((weak, alias("default_handler")));
void CAN0_2_hdlr(void) __attribute__((weak, alias("default_handler")));
void CAN0_3_hdlr(void) __attribute__((weak, alias("default_handler")));
void CAN0_4_hdlr(void) __attribute__((weak, alias("default_handler")));
void CAN0_5_hdlr(void) __attribute__((weak, alias("default_handler")));
void CAN0_6_hdlr(void) __attribute__((weak, alias("default_handler")));
void CAN0_7_hdlr(void) __attribute__((weak, alias("default_handler")));
void USIC0_0_hdlr(void) __attribute__((weak, alias("default_handler")));
void USIC0_1_hdlr(void) __attribute__((weak, alias("default_handler")));
void USIC0_2_hdlr(void) __attribute__((weak, alias("default_handler")));
void USIC0_3_hdlr(void) __attribute__((weak, alias("default_handler")));
void USIC0_4_hdlr(void) __attribute__((weak, alias("default_handler")));
void USIC0_5_hdlr(void) __attribute__((weak, alias("default_handler")));
void USIC1_0_hdlr(void) __attribute__((weak, alias("default_handler")));
void USIC1_1_hdlr(void) __attribute__((weak, alias("default_handler")));
void USIC1_2_hdlr(void) __attribute__((weak, alias("default_handler")));
void USIC1_3_hdlr(void) __attribute__((weak, alias("default_handler")));
void USIC1_4_hdlr(void) __attribute__((weak, alias("default_handler")));
void USIC1_5_hdlr(void) __attribute__((weak, alias("default_handler")));
void USIC2_0_hdlr(void) __attribute__((weak, alias("default_handler")));
void USIC2_1_hdlr(void) __attribute__((weak, alias("default_handler")));
void USIC2_2_hdlr(void) __attribute__((weak, alias("default_handler")));
void USIC2_3_hdlr(void) __attribute__((weak, alias("default_handler")));
void USIC2_4_hdlr(void) __attribute__((weak, alias("default_handler")));
void USIC2_5_hdlr(void) __attribute__((weak, alias("default_handler")));
void LEDTS0_0_hdlr(void) __attribute__((weak, alias("default_handler")));
void FCE0_0_hdlr(void) __attribute__((weak, alias("default_handler")));
void GPDMA0_0_hdlr(void) __attribute__((weak, alias("default_handler")));
void SDMMC0_0_hdlr(void) __attribute__((weak, alias("default_handler")));
void USB0_0_hdlr(void) __attribute__((weak, alias("default_handler")));
void ETH0_0_hdlr(void) __attribute__((weak, alias("default_handler")));
void GPDMA1_0_hdlr(void) __attribute__((weak, alias("default_handler")));

__attribute((section(".isr_vector")))
u32 *isr_vectors[] = {
    (u32 *) &_estack,
    (u32 *) reset_handler,
    (u32 *) nmi_handler,
    (u32 *) hardfault_handler,
    (u32 *) memmanage_handler,
    (u32 *) busfault_handler,
    (u32 *) usagefault_handler,
    0,
    0,
    0,
    0,
    (u32 *) svc_handler,
    0,
    0,
    (u32 *) pendsv_handler,
    (u32 *) systick_handler,
    (u32 *) SCU_0_hdlr,
    (u32 *) ERU0_0_hdlr,
    (u32 *) ERU0_1_hdlr,
    (u32 *) ERU0_2_hdlr,
    (u32 *) ERU0_3_hdlr,
    (u32 *) ERU1_0_hdlr,
    (u32 *) ERU1_1_hdlr,
    (u32 *) ERU1_2_hdlr,
    (u32 *) ERU1_3_hdlr,
    (u32 *) PMU0_0_hdlr,
    (u32 *) VADC0_C0_0_hdlr,
    (u32 *) VADC0_C0_1_hdlr,
    (u32 *) VADC0_C0_2_hdlr,
    (u32 *) VADC0_C0_3_hdlr,
    (u32 *) VADC0_G0_0_hdlr,
    (u32 *) VADC0_G0_1_hdlr,
    (u32 *) VADC0_G0_2_hdlr,
    (u32 *) VADC0_G0_3_hdlr,
    (u32 *) VADC0_G1_0_hdlr,
    (u32 *) VADC0_G1_1_hdlr,
    (u32 *) VADC0_G1_2_hdlr,
    (u32 *) VADC0_G1_3_hdlr,
    (u32 *) VADC0_G2_0_hdlr,
    (u32 *) VADC0_G2_1_hdlr,
    (u32 *) VADC0_G2_2_hdlr,
    (u32 *) VADC0_G2_3_hdlr,
    (u32 *) VADC0_G3_0_hdlr,
    (u32 *) VADC0_G3_1_hdlr,
    (u32 *) VADC0_G3_2_hdlr,
    (u32 *) VADC0_G3_3_hdlr,
    (u32 *) DSD0_M_0_hdlr,
    (u32 *) DSD0_M_1_hdlr,
    (u32 *) DSD0_M_2_hdlr,
    (u32 *) DSD0_M_3_hdlr,
    (u32 *) DSD0_A_4_hdlr,
    (u32 *) DSD0_A_5_hdlr,
    (u32 *) DSD0_A_6_hdlr,
    (u32 *) DSD0_A_7_hdlr,
    (u32 *) DAC0_0_hdlr,
    (u32 *) DAC0_1_hdlr,
    (u32 *) CCU40_0_hdlr,
    (u32 *) CCU40_1_hdlr,
    (u32 *) CCU40_2_hdlr,
    (u32 *) CCU40_3_hdlr,
    (u32 *) CCU41_0_hdlr,
    (u32 *) CCU41_1_hdlr,
    (u32 *) CCU41_2_hdlr,
    (u32 *) CCU41_3_hdlr,
    (u32 *) CCU42_0_hdlr,
    (u32 *) CCU42_1_hdlr,
    (u32 *) CCU42_2_hdlr,
    (u32 *) CCU42_3_hdlr,
    (u32 *) CCU43_0_hdlr,
    (u32 *) CCU43_1_hdlr,
    (u32 *) CCU43_2_hdlr,
    (u32 *) CCU43_3_hdlr,
    (u32 *) CCU80_0_hdlr,
    (u32 *) CCU80_1_hdlr,
    (u32 *) CCU80_2_hdlr,
    (u32 *) CCU80_3_hdlr,
    (u32 *) CCU81_0_hdlr,
    (u32 *) CCU81_1_hdlr,
    (u32 *) CCU81_2_hdlr,
    (u32 *) CCU81_3_hdlr,
    (u32 *) POSIF0_0_hdlr,
    (u32 *) POSIF0_1_hdlr,
    (u32 *) POSIF1_0_hdlr,
    (u32 *) POSIF1_1_hdlr,
    (u32 *) CAN0_0_hdlr,
    (u32 *) CAN0_1_hdlr,
    (u32 *) CAN0_2_hdlr,
    (u32 *) CAN0_3_hdlr,
    (u32 *) CAN0_4_hdlr,
    (u32 *) CAN0_5_hdlr,
    (u32 *) CAN0_6_hdlr,
    (u32 *) CAN0_7_hdlr,
    (u32 *) USIC0_0_hdlr,
    (u32 *) USIC0_1_hdlr,
    (u32 *) USIC0_2_hdlr,
    (u32 *) USIC0_3_hdlr,
    (u32 *) USIC0_4_hdlr,
    (u32 *) USIC0_5_hdlr,
    (u32 *) USIC1_0_hdlr,
    (u32 *) USIC1_1_hdlr,
    (u32 *) USIC1_2_hdlr,
    (u32 *) USIC1_3_hdlr,
    (u32 *) USIC1_4_hdlr,
    (u32 *) USIC1_5_hdlr,
    (u32 *) USIC2_0_hdlr,
    (u32 *) USIC2_1_hdlr,
    (u32 *) USIC2_2_hdlr,
    (u32 *) USIC2_3_hdlr,
    (u32 *) USIC2_4_hdlr,
    (u32 *) USIC2_5_hdlr,
    (u32 *) LEDTS0_0_hdlr,
    (u32 *) FCE0_0_hdlr,
    (u32 *) GPDMA0_0_hdlr,
    (u32 *) SDMMC0_0_hdlr,
    (u32 *) USB0_0_hdlr,
    (u32 *) ETH0_0_hdlr,
    (u32 *) GPDMA1_0_hdlr
};
